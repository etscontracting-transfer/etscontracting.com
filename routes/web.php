<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/about', function () {
    return view('about');
})->name('about');

Route::get('/services', function () {
    return view('services');
})->name('services');

Route::get('/clients', function () {
    return view('clients');
})->name('clients');

Route::get('/links', function () {
    return view('links');
})->name('links');

Route::get('/contact', function () {
    return view('contact');
})->name('contact');

Route::get('/team', function () {
    return view('team');
})->name('team');

Route::get('/approvals', function () {
    return view('approvals');
})->name('approvals');

Route::get('/licenses', function () {
    return view('licenses');
})->name('licenses');
