<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.analytics')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>ETS Contracting, Inc.</title>

    <script type="text/javascript">
        <!--
        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
        //-->
    </script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('{{ asset('images/nav/home_on.png') }}','{{ asset('images/nav/about_on.png') }}','{{ asset('images/nav/services_on.png') }}','{{ asset('images/nav/clients_on.png') }}','{{ asset('images/nav/links_on.png') }}','{{ asset('images/nav/contact_on.png') }}')">
<div id="thebox">
    <div id="header"><img src="{{ asset('images/logo.png') }}" width="986" height="105" border="0" usemap="#Map" />
        <map name="Map" id="Map">
            <area shape="rect" coords="8,18,382,83" href="{{ route('home') }}" />
        </map>
    </div>
    <div id="navigation">
        <div id="nav_inner"><a href="{{ route('home') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('home','','{{ asset('images/nav/home_on.png') }}',1)"><img src="{{ asset('images/nav/home_off.png') }}" alt="Home Button" name="home" width="124" height="38" border="0" id="home" /></a><a href="{{ route('about') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('about','','{{ asset('images/nav/about_on.png') }}',1)"><img src="{{ asset('images/nav/about_off.png') }}" alt="About Us Button" name="about" width="163" height="38" border="0" id="about" /></a><a href="{{ route('services') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('services','','{{ asset('images/nav/services_on.png') }}',1)"><img src="{{ asset('images/nav/services_off.png') }}" name="services" width="191" height="38" border="0" id="services" /></a><a href="{{ route('clients') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('clients','','{{ asset('images/nav/clients_on.png') }}',1)"><img src="{{ asset('images/nav/clients_off.png') }}" name="clients" width="156" height="38" border="0" id="clients" /></a><a href="{{ route('links') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('links','','{{ asset('images/nav/links_on.png') }}',1)"><img src="{{ asset('images/nav/links_off.png') }}" name="links" width="147" height="38" border="0" id="links" /></a><a href="{{ route('contact') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('contact','','{{ asset('images/nav/contact_on.png') }}',1)"><img src="{{ asset('images/nav/contact_on.png') }}" name="contact" width="190" height="38" border="0" id="contact" /></a></div>
    </div>
    <div id="body"><br />
        <table width="960" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="105" align="left" valign="top" scope="row"><span class="txTitle">CONTACT US</span></td>
                <td width="10" rowspan="3" align="left" valign="top" scope="row"><span class="mText"><br />
        </span></td>
                <td width="421" rowspan="3" align="left" valign="top" class="mText" scope="row">
                    Please feel free to contact us with any questions or concerns. Our team of professionals is ready to assist you with all aspects of your project, from project design to completion.
                    <br />
                    <table cellspacing="20px" id="team">
                        <tr><td width="180">
                                <strong>ETS Contracting, Inc.</strong><br />
                                160 Clay Street<br />
                                Brooklyn, NY 11222<br />
                                T: 718.706.6300<br />
                                F: 718.706.1032<br />
                                E: <i style="color: #BC143B">mail[at]etscontracting.com</i>
                            </td>
                        </tr>
                    </table><table width="420" border="0" cellspacing="0" cellpadding="0" id="team">
                        <tr>
                            <td width="200" align="left" valign="top"><p><strong>Administrative/Management</strong></p>
                                <p>Renata Buczek<br />
                                    <i style="color: #BC143B">Renata[at]etscontracting.com</i>
                                </p>

                                <p>Monika Pieciak<br />
                                    <i style="color: #BC143B">Monika[at]etscontracting.com</i>
                                </p>

                                <p>Iwona Igras<br />
                                    <i style="color: #BC143B">Iwona[at]etscontracting.com</i>
                                </p>

                                <p>Aneta Filipkowski<br />
                                    <i style="color: #BC143B">Aneta[at]etscontracting.com</i>
                                </p>

                                <p>Diana Riuz<br />
                                    <i style="color: #BC143B">Diana[at]etscontracting.com</i><br>
                                </p>

                                <p>&nbsp;</p>
                                <p><strong>Project Coordination/Operation</strong></p>
                                <p>Marian Jawien<br />
                                    <i style="color: #BC143B">Marian[at]etscontracting.com</i>
                                </p>
                                <p>Lukasz Warchol<br />
                                    <i style="color: #BC143B">Lucas[at]etscontracting.com</i></p>

                                <p>Jan Sidor<br />
                                    <i style="color: #BC143B">Jsidor[at]etscontracting.com</i>
                                </p>

                            </td>
                            <td width="20"></td>
                            <td width="200" align="left" valign="top"><p><strong>Sales/Estimating</strong></p>
                                <p>Robert Middleton<br />
                                    <i style="color: #BC143B">Robert[at]etscontracting.com</i></p>
                                <p>Richie Smith<br />
                                    <i style="color: #BC143B">Richie[at]etscontracting.com</i></p>
                                <p>Lech Sawicki<br />
                                    <i style="color: #BC143B">Lech[at]etscontracting.com</i></p>
                                <p>Thomas Ahern<br />
                                    <i style="color: #BC143B">Thomas[at]etscontracting.com</i></p>
                                <p>George Salame<br />
                                    <i style="color: #BC143B">George[at]etscontracting.com</i></p></td>
                        </tr>
                    </table></td>
                <td width="11" rowspan="3" align="left" valign="top" scope="row">&nbsp;</td>
                <td width="413" rowspan="3" align="right" valign="top" scope="row">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3023.1563980423703!2d-73.951171!3d40.736584!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c2593bcee8d61f%3A0xc576d1649b1516e6!2s160+Clay+St!5e0!3m2!1sen!2sus!4v1395160266298" width="400" height="300" frameborder="0" style="border:0"></iframe></td>
            </tr>
            <tr>
                <td width="105" align="left" valign="top" class="subNav" scope="row">&nbsp;</td>
            </tr>
            <tr>
                <td width="105" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
        </table>
        <p><br />
        </p>
    </div>
    <div id="footer"><img src="{{ asset('images/line_bot.png') }}" width="986" height="38" /><br />
        <br />
        ETS Contracting, Inc. 160 Clay Street, Brooklyn, NY 11222 | T. 718.706.6300 | F. 718.706.1032 | E. <span style="color: #BC143B;">mail[at]etscontracting.com</span><br />
        Copyright &copy; 2004 - <script>document.write(new Date().getFullYear())</script> ETS Contracting | Site Design and Development by <a href="http://www.bermangrp.com">The Berman Group</a><br />
    </div>
    <div id="bottom"><img src="{{ asset('images/bottom.png') }}" width="1026" height="26" /></div>
</div>
</body>
</html>
