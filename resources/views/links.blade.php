<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.analytics')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>ETS Contracting, Inc.</title>

    <script type="text/javascript">
        <!--
        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
        //-->
    </script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
</head>

<body onload="MM_preloadImages('{{ asset('images/nav/home_on.png') }}','{{ asset('images/nav/about_on.png') }}','{{ asset('images/nav/services_on.png') }}','{{ asset('images/nav/clients_on.png') }}','{{ asset('images/nav/links_on.png') }}','{{ asset('images/nav/contact_on.png') }}')">
<div id="thebox">
    <div id="header"><img src="{{ asset('images/logo.png') }}" width="986" height="105" border="0" usemap="#Map" />
        <map name="Map" id="Map">
            <area shape="rect" coords="8,18,382,83" href="{{ route('home') }}" />
        </map>
    </div>
    <div id="navigation">
        <div id="nav_inner"><a href="{{ route('home') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('home','','{{ asset('images/nav/home_on.png') }}',1)"><img src="{{ asset('images/nav/home_off.png') }}" alt="Home Button" name="home" width="124" height="38" border="0" id="home" /></a><a href="{{ route('about') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('about','','{{ asset('images/nav/about_on.png') }}',1)"><img src="{{ asset('images/nav/about_off.png') }}" alt="About Us Button" name="about" width="163" height="38" border="0" id="about" /></a><a href="{{ route('services') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('services','','{{ asset('images/nav/services_on.png') }}',1)"><img src="{{ asset('images/nav/services_off.png') }}" name="services" width="191" height="38" border="0" id="services" /></a><a href="{{ route('clients') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('clients','','{{ asset('images/nav/clients_on.png') }}',1)"><img src="{{ asset('images/nav/clients_off.png') }}" name="clients" width="156" height="38" border="0" id="clients" /></a><a href="{{ route('contact') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('links','','{{ asset('images/nav/links_on.png') }}',1)"><img src="{{ asset('images/nav/links_on.png') }}" name="links" width="147" height="38" border="0" id="links" /></a><a href="{{ route('contact') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('contact','','{{ asset('images/nav/contact_on.png') }}',1)"><img src="{{ asset('images/nav/contact_off.png') }}" name="contact" width="190" height="38" border="0" id="contact" /></a></div>
    </div>
    <div id="body"><br />
        <table width="960" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="105" align="left" valign="top" scope="row"><span class="txTitle">USEFUL LINKS</span> <br /></td>
                <td width="10" rowspan="3" align="left" valign="top" scope="row"><span class="mText"><br />
          </span></td>
                <td width="621" rowspan="3" align="left" valign="top" scope="row"><span class="mText" id="team">
            <a href="http://www.etscontracting.com/media/20140311103533045.pdf" target="_blank">Protections for Reporting Fraud in New York</a>
            <br/><br/>
            <a href="http://lwd.dol.state.nj.us/" target="_blank">State of New Jersey Department of Labor and Workforce Development</a>
            <br/><br/>
            <a href="http://www.asse.org/" target="_blank">American Society of Safety Engineers</a>
            <br/><br/>
            <a target="_blank" href="http://www.epa.gov/">Environmental Protection Agency</a>
            <br /><br />
            <a target="_blank" href="http://www.osha.gov/">Occupation Safety and Health Administration</a>
            <br /><br />
            <a target="_blank" href="http://www.ecanyc.org">Environmental Contractors Association</a>
            <br /><br />
            <a target="_blank" href="http://www.cdc.gov/niosh/">The National Institute of Occupational Safety and Health</a>
            <br /><br />
            <a target="_blank" href="http://www.labor.ny.gov/home/">New York State Department of Labor</a>
            <br /><br />
            <a target="_blank" href="http://www.ansi.org/">American National Standards Institute</a>
            <br /><br />
            <a target="_blank" href="http://www.astm.org/">American Society for Testing and Materials</a>
            <br /><br />
            <a target="_blank" href="http://www.nyc.gov/html/dob/html/home/home.shtml">New York City Department of Buildings</a>
            <br /><br />
            <a target="_blank" href="http://www.nyc.gov/html/dep/html/home/home.shtml">New York City Department of Environmental Protection</a>
            <br /><br />
            <a target="_blank" href="http://portal.hud.gov/hudportal/HUD?src=/program_offices/healthy_homes">US Department of Housing and Urban Development Lead Guide</a>
          </span><br /></td>
                <td width="11" rowspan="3" align="left" valign="top" scope="row">&nbsp;</td>
                <td width="213" rowspan="3" align="right" valign="top" scope="row"><img src="{{ asset('images/about/right.jpg') }}" alt="" width="197" height="338" /><br /></td>
            </tr>
            <tr>
                <td width="105" align="left" valign="top" class="subNav" scope="row">&nbsp;</td>
            </tr>
            <tr>
                <td width="105" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
        </table>
        <p><br />
        </p>
    </div>
    <div id="footer"><img src="{{ asset('images/line_bot.png') }}" width="986" height="38" /><br />
        <br />
        ETS Contracting, Inc. 160 Clay Street, Brooklyn, NY 11222 | T. 718.706.6300 | F. 718.706.1032 | E. <span style="color: #BC143B;">mail[at]etscontracting.com</span><br />
        Copyright &copy; 2004 - <script>document.write(new Date().getFullYear())</script> ETS Contracting | Site Design and Development by <a href="http://www.bermangrp.com">The Berman Group</a><br />
    </div>
    <div id="bottom"><img src="{{ asset('images/bottom.png') }}" width="1026" height="26" /></div>
</div>
</body>
</html>
