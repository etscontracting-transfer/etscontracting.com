<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.analytics')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>ETS Contracting, Inc.</title>

    <script type="text/javascript">
        <!--
        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
        //-->
    </script>
    <link href="{{ asset('css/style.css') }}" rel="stylesheet" type="text/css" />
</head>

<div id="thebox">
    <div id="header"><img src="{{ asset('images/logo.png') }}" width="986" height="105" border="0" usemap="#Map" />
        <map name="Map" id="Map">
            <area shape="rect" coords="8,18,382,83" href="{{ route('home') }}" />
        </map>
    </div>
    <div id="navigation">
        <div id="nav_inner"><a href="{{ route('home') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('home','','{{ asset('images/nav/home_on.png') }}',1)"><img src="{{ asset('images/nav/home_off.png') }}" alt="Home Button" name="home" width="124" height="38" border="0" id="home" /></a><a href="{{ route('about') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('about','','{{ asset('images/nav/about_on.png') }}',1)"><img src="{{ asset('images/nav/about_off.png') }}" alt="About Us Button" name="about" width="163" height="38" border="0" id="about" /></a><a href="{{ route('services') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('services','','{{ asset('images/nav/services_on.png') }}',1)"><img src="{{ asset('images/nav/services_on.png') }}" name="services" width="191" height="38" border="0" id="services" /></a><a href="{{ route('clients') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('clients','','{{ asset('images/nav/clients_on.png') }}',1)"><img src="{{ asset('images/nav/clients_off.png') }}" name="clients" width="156" height="38" border="0" id="clients" /></a><a href="{{ route('links') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('links','','{{ asset('images/nav/links_on.png') }}',1)"><img src="{{ asset('images/nav/links_off.png') }}" name="links" width="147" height="38" border="0" id="links" /></a><a href="{{ asset('contact') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('contact','','{{ asset('images/nav/contact_on.png') }}',1)"><img src="{{ asset('images/nav/contact_off.png') }}" name="contact" width="190" height="38" border="0" id="contact" /></a></div>
    </div>
    <div id="body"><br />
        <table width="960" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="125" align="left" valign="top" scope="row">
                    <span class="txTitle">SERVICES</span><br />
                </td>
                <td width="10" rowspan="3" align="left" valign="top" scope="row">
            <span class="mText">
              <br />
            </span>
                </td>
                <td width="601" rowspan="3" align="left" valign="top" class="mText" scope="row">
                    <p>
                        ETS Contracting specializes in <strong>environmental remediation</strong> of public and private facilities throughout the Tri-State area. We work closely with our clients to ensure that their projects get handled in a timely and professional manner. No matter the size or complexity, every project is handled in a safe, productive and cost-effective manner.
                    </p>
                    <p>
                        Fields of expertise include:
                    </p>
                    <p>
                        <strong><em>Asbestos Abatement</em></strong>
                        <br />
                        Many older commercial and residential buildings still contain asbestos, which was used in a variety of applications including, but not limited, to spray-applied flame retardant and thermal systems insulation. ETS has <strong>extensive experience</strong> in the asbestos abatement field, providing <strong>top quality service</strong> to a wide spectrum of public and private facilities. Our project management team consists of specialists with years of experience and all required certifications. They are proficient in tackling an array of challenges and routine asbestos abatement issues.
                    </p>
                    <p>
                        <strong><em>Lead</em></strong>
                        <br />
                        Regarded as the number one environmental threat to the health of children in the United States, lead is more than a harmful pollutant. The field of lead abatement requires particular sensitivity, abatement experience, and proper training. All of ETS lead abatement workers are certified as required by local laws. ETS approaches these projects with the upmost care and attention, very closely overseeing each effort with the sternest adherence to all state and local laws.
                    </p>
                    <p>
                        <strong><em>Microbial</em></strong>
                        <br />
                        Microbial contaminants are incredibly harmful to people in varying gestation periods and demand prompt and thorough removal. <strong>ETS customizes </strong>the response to individual occurrences of microbial contamination, determining the assignment of personnel in the most <strong>cost-effective manner</strong> for the client. A thorough survey of the site provides the client with an in-depth analysis of the extent of contamination before a specialist determines the various abatement options and best possible course of action.
                    </p>
                    <p>
                        <strong><em>Building Service – Maintenance</em></strong>
                        <br/>
                        ETS performs cleaning and building service maintenance while working, and the general post project cleaning services. The on-site team will make sure our services meet timelines and expectations. We are committed to advanced and cost-effective methods and techniques to reduce the disruption of everyday operations of our clients.
                    </p>
                    <p>
                        <strong><em>Additional Services</em></strong>
                    </p>
                    <ul>
                        <li>HVAC Duct Cleaning</li>
                        <li>Drying Services</li>
                        <li>Hazardous Waste Management</li>
                        <li>24 Hour Emergency Response</li>
                        <li>Re-insulation</li>
                        <li>Fireproofing</li>
                    </ul>
                </td>
                <td width="11" rowspan="3" align="left" valign="top" scope="row">&nbsp;</td>
                <td width="213" rowspan="3" align="right" valign="top" scope="row">
                    <img src="{{ asset('images/about/right.jpg') }}" alt="" width="197" height="338" />
                    <br />
                </td>
            </tr>
            <tr>
                <td width="105" align="left" valign="top" class="subNav" scope="row">&nbsp;</td>
            </tr>
            <tr>
                <td width="105" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
        </table>
        <p>
            <br />
        </p>
    </div>
    <div id="footer"><img src="{{ asset('images/line_bot.png') }}" width="986" height="38" /><br />
        <br />
        ETS Contracting, Inc. 160 Clay Street, Brooklyn, NY 11222 | T. 718.706.6300 | F. 718.706.1032 | E. <span style="color: #BC143B;">mail[at]etscontracting.com</span><br />
        Copyright &copy; 2004 - <script>document.write(new Date().getFullYear())</script> ETS Contracting | Site Design and Development by <a href="http://www.bermangrp.com">The Berman Group</a><br />
    </div>
    <div id="bottom">
        <img src="{{ asset('images/bottom.png') }}" width="1026" height="26" />
    </div>
</div>
</body>
</html>
