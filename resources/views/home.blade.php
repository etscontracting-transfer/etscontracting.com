<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    @include('includes.analytics')
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>ETS Contracting, Inc.</title>

    <script src="{{ asset('Scripts/swfobject_modified.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        <!--
        function MM_swapImgRestore() { //v3.0
            var i,x,a=document.MM_sr; for(i=0;a&&i<a.length&&(x=a[i])&&x.oSrc;i++) x.src=x.oSrc;
        }
        function MM_preloadImages() { //v3.0
            var d=document; if(d.images){ if(!d.MM_p) d.MM_p=new Array();
                var i,j=d.MM_p.length,a=MM_preloadImages.arguments; for(i=0; i<a.length; i++)
                    if (a[i].indexOf("#")!=0){ d.MM_p[j]=new Image; d.MM_p[j++].src=a[i];}}
        }

        function MM_findObj(n, d) { //v4.01
            var p,i,x;  if(!d) d=document; if((p=n.indexOf("?"))>0&&parent.frames.length) {
                d=parent.frames[n.substring(p+1)].document; n=n.substring(0,p);}
            if(!(x=d[n])&&d.all) x=d.all[n]; for (i=0;!x&&i<d.forms.length;i++) x=d.forms[i][n];
            for(i=0;!x&&d.layers&&i<d.layers.length;i++) x=MM_findObj(n,d.layers[i].document);
            if(!x && d.getElementById) x=d.getElementById(n); return x;
        }

        function MM_swapImage() { //v3.0
            var i,j=0,x,a=MM_swapImage.arguments; document.MM_sr=new Array; for(i=0;i<(a.length-2);i+=3)
                if ((x=MM_findObj(a[i]))!=null){document.MM_sr[j++]=x; if(!x.oSrc) x.oSrc=x.src; x.src=a[i+2];}
        }
        //-->
    </script>
    <link href="{{ asset('css/index.css') }}" rel="stylesheet" type="text/css" />

    <style type="text/css">


        #ajaxticker1{

        }

        #ajaxticker1 div{
        }

        .someclass{ //class to apply to your scroller(s) if desired
        }

    </style>

    <script src="{{ asset('js/ajaxticker.js') }}" type="text/javascript"></script>

</head>

<body onload="MM_preloadImages('{{ asset('images/nav/home_on.png') }}','{{ asset('images/nav/about_on.png') }}','{{ asset('images/nav/services_on.png') }}','{{ asset('images/nav/clients_on.png') }}','{{ asset('images/nav/links_on.png') }}','{{ asset('images/nav/contact_on.png') }}')">
<div id="thebox">
    <div id="header"><img src="{{ asset('images/logo.png') }}" width="986" height="105" border="0" usemap="#Map" />
        <map name="Map" id="Map">
            <area shape="rect" coords="8,18,382,83" href="{{ route('home') }}" />
        </map>
    </div>
    <div id="navigation">
        <div id="nav_inner"><a href="#" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('home','','{{ asset('images/nav/home_on.png') }}',1)"><img src="{{ asset('images/nav/home_on.png') }}" alt="Home Button" name="home" width="124" height="38" border="0" id="home" /></a><a href="{{ route('about') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('about','','{{ asset('images/nav/about_on.png') }}',1)"><img src="{{ asset('images/nav/about_off.png') }}" alt="About Us Button" name="about" width="163" height="38" border="0" id="about" /></a><a href="{{ route('services') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('services','','{{ asset('images/nav/services_on.png') }}',1)"><img src="{{ asset('images/nav/services_off.png') }}" name="services" width="191" height="38" border="0" id="services" /></a><a href="{{ route('clients') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('clients','','{{ asset('images/nav/clients_on.png') }}',1)"><img src="{{ asset('images/nav/clients_off.png') }}" name="clients" width="156" height="38" border="0" id="clients" /></a><a href="{{ route('links') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('links','','{{ asset('images/nav/links_on.png') }}',1)"><img src="{{ asset('images/nav/links_off.png') }}" name="links" width="147" height="38" border="0" id="links" /></a><a href="{{ route('contact') }}" onmouseout="MM_swapImgRestore()" onmouseover="MM_swapImage('contact','','{{ asset('images/nav/contact_on.png') }}',1)"><img src="{{ asset('images/nav/contact_off.png') }}" name="contact" width="190" height="38" border="0" id="contact" /></a></div>
    </div>
    <div id="body"><br />
        <table width="960" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td width="5" align="left" valign="top" bgcolor="#F4F4F4" scope="row">&nbsp;</td>
                <td width="740" align="left" valign="top" bgcolor="#F4F4F4" scope="row"><span class="txTitle">OUR COMMITMENT</span><br />
                    <span class="mText">We strive for complete customer satisfaction by providing knowledgeable personnel and quality work while maintaining the highest professional and safety standards. <br />
              <br />
            </span></td>
                <td width="20" align="left" valign="top" scope="row">&nbsp;</td>
                <td rowspan="3" align="left" valign="top" scope="row">
                    <img src="{{ asset('images/about/right.jpg') }}">
                </td>
            </tr>
            <tr>
                <td width="5" align="left" valign="top" bgcolor="#E6E6E6" scope="row">&nbsp;</td>
                <td width="440" align="left" valign="top" bgcolor="#E6E6E6" scope="row"><span class="txTitle">HEALTH AND SAFETY</span><br />
                    <span class="mText">ETS Contracting, Inc. acknowledges that our employees are our most valuable assets; their well being and safety are
               always a top priority. We strive to eliminate all possible sources of on the job injuries, public harm, and
               pollution by implementing all feasible precautions. ETS prides itself with adhering to all applicable Local,
               State, and Federal safety standards. We do so through thorough work site inspections, as well as proper
               site supervision.<br />
               <br />
             </span></td>
                <td width="20" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
            <tr>
                <td width="5" align="left" valign="top" bgcolor="#F4F4F4" scope="row">&nbsp;</td>
                <td width="440" align="left" valign="top" bgcolor="#F4F4F4" scope="row"><span class="txTitle">INSURANCE DETAILS<br />
            </span><span class="mText">
            ETS Contracting is insured by an A+15 rated insurance company as determined by A.M. Best Company.<br />
            ETS carries the following coverages:
            <ul style="list-style-type:none;margin-left:0;padding-left:1em;text-indent:-10px;">
              <li>&#150;&nbsp;&nbsp;General Liability / Pollution Liability Per Occurrence</li>
              <li>&#150;&nbsp;&nbsp;General Liability / Pollution Liability Aggregate</li>
              <li>&#150;&nbsp;&nbsp;Automotive Liability</li>
              <li>&#150;&nbsp;&nbsp;Umbrella Coverage Including Pollution Liability</li>
              <li>&#150;&nbsp;&nbsp;Statutory Workers Compensation and Disability Coverage</li>
            </ul>
            Certificate of Insurance will be provided as per requested.<br />
            <br />

          </span></td>
                <td width="20" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
            <tr>
                <td width="5" align="left" valign="top" bgcolor="#E6E6E6" scope="row">&nbsp;</td>
                <td width="440" align="left" valign="top" bgcolor="#E6E6E6" scope="row"><span class="txTitle">BONDS</span><br />
                    <span class="mText">ETS  retains a sufficient bonding capacity with an A-Rated and T-listed company.<br />
              <br />
            </span></td>
                <td width="20" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
            <tr>
                <td width="5" align="left" valign="top" bgcolor="#F4F4F4" scope="row">&nbsp;</td>
                <td width="440" align="left" valign="top" bgcolor="#F4F4F4" scope="row"><span class="txTitle">MTA LINKS<br />
            </span><span class="mText">
            <ul style="list-style-type:none;margin-left:0;padding-left:1em;text-indent:-10px;">
              <li>&#150;&nbsp;&nbsp;<a href="{{ asset('media/20140311103533045.pdf') }}" target="_blank">Protections for Reporting Fraud in New York,</a><br />
                <br />
              </li>
            </ul>
            </span></td>
                <td width="20" align="left" valign="top" scope="row">&nbsp;</td>
            </tr>
        </table>
        <br />
    </div>
    <div id="footer"><img src="{{ asset('images/line_bot.png') }}" width="986" height="38" /><br />
        <br />
        ETS Contracting, Inc. 160 Clay Street, Brooklyn, NY 11222 | T. 718.706.6300 | F. 718.706.1032 | E. <span style="color: #BC143B;">mail[at]etscontracting.com</span><br />
        Copyright &copy; 2004 - <script>document.write(new Date().getFullYear())</script> ETS Contracting | Site Design and Development by <a href="http://www.bermangrp.com">The Berman Group</a><br />
    </div>
    <div id="bottom"><img src="{{ asset('images/bottom.png') }}" width="1026" height="26" /></div>
</div>
<script type="text/javascript">
    <!--
    swfobject.registerObject("FlashID");
    //-->
</script>
</body>
</html>
